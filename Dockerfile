FROM dokken/centos-8
USER root:root
ADD test.sh ~/test.sh
ENTRYPOINT ["/bin/bash", "~/test.sh"]